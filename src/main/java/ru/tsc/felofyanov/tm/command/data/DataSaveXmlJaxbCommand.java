package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.Domain;
import ru.tsc.felofyanov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public final class DataSaveXmlJaxbCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file(JAXB)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML(JAXB)]");
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_XML);
        marshaller.marshal(domain, fileOutputStream);

        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
