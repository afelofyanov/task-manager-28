package ru.tsc.felofyanov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.model.IHasStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<IHasStatus> {

    INSTANCE;

    @Override
    public int compare(@Nullable final IHasStatus iHasStatus, @Nullable final IHasStatus t1) {
        if (iHasStatus == null || t1 == null) return 0;
        if (iHasStatus.getStatus() == null || t1.getStatus() == null) return 0;
        return iHasStatus.getStatus().compareTo(t1.getStatus());
    }
}
