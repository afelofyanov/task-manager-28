package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return models.stream()
                .filter(task -> projectId.equals(task.getProjectId())
                        && userId.equals(task.getUserId())
                        && task.getProjectId() != null
                        && task.getUserId() != null)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task create(final @Nullable String userId, final @NotNull String name, final @NotNull String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }
}
