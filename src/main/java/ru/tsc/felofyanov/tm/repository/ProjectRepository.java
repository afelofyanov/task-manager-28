package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return add(project);
    }
}
