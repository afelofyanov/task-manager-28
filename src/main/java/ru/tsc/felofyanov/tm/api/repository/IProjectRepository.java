package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @NotNull
    Project create(@Nullable String userId, @NotNull String name);

    @NotNull
    Project create(@Nullable String userId, @NotNull String name, @NotNull String description);
}
